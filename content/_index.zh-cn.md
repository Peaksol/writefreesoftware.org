+++
title = "编写自由软件"
+++
{{< block "grid-2" >}}
{{< column >}}

# 共筑软件，普惠大众

自由软件是一项全球运动，携手构造世界最强的软件，同时尊重用户及作者的基本权利与自由 &mdash;
所谓自由，即指**使用**、**研究**、**改进**和**分享**软件的自由。

{{< button "learn" "什么是自由软件?" >}}{{< button "learn/participate" "开始行动" >}}
{{< /column >}}

{{< column >}}
![一群黑客在 FOSDEM 的图片](/images/banner.jpg)
<small>摄像：Flo Köhler, CC-BY 3.0</small>
{{< /column >}}

{{< /block >}}
