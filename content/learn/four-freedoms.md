---
title: The four freedoms
weight: -10
---

<blockquote>
  <ol start="0">
    <li>
      The freedom to <strong>use</strong> the software for any purpose.
    </li>
    <li>
      The freedom to <strong>study</strong> and <strong>improve</strong> the
      software.
    </li>
    <li>
      The freedom to <strong>share</strong> the software.
    </li>
    <li>
      The freedom to <strong>collaborate</strong> on the software.
    </li>
  </ol>
</blockquote>

Let's examine each of the four freedoms in more detail.

## 0: Use the software

The "zeroth" freedom guarantees the right for everyone to **use** the software
for any purpose. You are entitled to the use of any free software for any
purpose, including commercial use -- counter-intuitively, you can sell free
software. You can also incorporate free software into your own work, but be
careful -- there are some gotchas here, which we cover under [use &
reuse](/learn/participate/derived-works/).

{{< tip >}}
In free software lingo, this is often called the "non-discrimination"
requirement.
{{< /tip >}}

## 1: Study and improve the software

The first freedom guarantees the right to **study** the program's behavior. You
have a right to understand how the software you depend on works! In order to
facilitate this, the source code must be included with the software. The
publishers of free software cannot obfuscate the source code, forbid reverse
engineering, or otherwise prevent you from making use of it.

Moreover, you have a right to **improve** the source code. You are not only
given the right to read the source code of free software, but also to change it
to better suit your needs.

## 2: Share the software

The second freedom guarantees the right to **share** free software with others.
If you have a copy of some free software, you can give it to your friends, or
publish it on your website, or bundle it with other software and share it as
part of something bigger. You can also share your improvements to it -- then
it's better for everyone!

{{< tip >}}
You can charge a fee to those you share the software with, for instance to cover
the costs of bandwidth. Note however that the recipient is entitled to the right
to share it themselves, without paying back any royalties.
{{< /tip >}}

## 3: Collaborate on the software

The third freedom guarantees one additional right: the right to **collaborate**
with others on improving the software. You can study, improve, and share the
source code, and the people you share it with can study, improve, and share it
right back with you. This is the foundation of the **Free Software Movement**: a
global community of software enthusiasts sharing and improving software
together.

{{< button "/learn/licenses" "Next: Free software licenses" "next-button" >}}
