---
title: About Free Software
weight: 1
---

> "Free software" is software developed & published according to principles
> which respect the essential rights and freedoms of its users and authors
> &mdash; the freedom to **use**, **study**, **improve**, and **share** the
> software. <wbr/>The **Free&nbsp;Software&nbsp;movement** is a global community
> of software developers and users who collaborate to build software according
> to these principles.

Any software is considered free software so long as it upholds the *four
essential freedoms*:

<ol start="0">
  <li>
    The freedom to <strong>use</strong> the software for any purpose.
  </li>
  <li>
    The freedom to <strong>study</strong> and <strong>improve</strong> the
    software.
  </li>
  <li>
    The freedom to <strong>share</strong> the software.
  </li>
  <li>
    The freedom to <strong>collaborate</strong> on the software.
  </li>
</ol>

Software which upholds these freedoms is **free software**. Software which does
not is **non-free**.

{{< button "/learn/four-freedoms" "Next: The four freedoms" "next-button" >}}

## What is "open source" software?

**Open Source** is a movement similar to the Free Software movement. The
movements differ mainly in their target audience: open source is more commercial
in its focus, and free software is more about the users. Nevertheless, the two
movements are closely related and often work together. Each movement provides a
different view of software freedom, but in practice nearly all software which is
considered free software is also considered open source and vice-versa. The Open
Source definition and the four freedoms are compatible with one another.

The two movements as a whole are often referred to as "free and open source
software", or "FOSS".

{{< tip >}}
Generally speaking, all open source software is free software, and vice versa.
{{< /tip >}}

You can learn more about Open Source at
[opensource.org](https://opensource.org/).

## What is "source available" software?

"Source available" software refers to any software for which the source code is
available, which may or may not uphold the four freedoms. It might limit
commercial use, restrict redistribution, prevent the user from modifying the
software, and so on. All free and open source software is source available, but
not all source available software is free software.

{{< tip "warning" >}}
"Source available" software is often non-free.
{{< /tip >}}
