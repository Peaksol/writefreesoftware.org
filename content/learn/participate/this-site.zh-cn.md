---
title: 贡献至该网站
weight: 1000
---

本网站本身是自由软件，其内容以及布局的源代码都托管在了 [SourceHut][0]，一个自由软件开发平台。点击链接，即可了解怎么把你的改进贡献给我们！本网站及其内容使用了 Creative Commons Attribution-ShareAlike 4.0 International（[CC-BY-SA 4.0][CC-BY-SA]）许可证，这是一份 copyleft 许可证。

[0]: https://sr.ht/~sircmpwn/writefreesoftware.org/
[CC-BY-SA]: https://creativecommons.org/licenses/by-sa/4.0/

本主题基于 Hugo <a href="https://github.com/onweru/compose" class="non-free" title="警告：这个连接指向 GitHub，一个非自由网站">compose</a> 主题，一份以 [MIT 许可证](https://mit-license.org/)授权的自由软件，版权所有 &copy; 2020 Weru.
