---
title: 对非软件资源进行授权
weight: 10
---

自由软件许可证，最适合许可的当然是软件。不过，自由软件项目也常常并入本身并非软件的媒体，比如说艺术作品以及文档。这些用例都有不同的许可证推荐。我们为非软件的媒体，推荐了一些许可证，它们本着自由软件精神而写，并且也兼容自由软件许可证。

## 知识共享（Creative Commons）

大多数多媒体资源 -- 图线、音频、视频、写作等 -- 都适合使用 [Creative Commons][0] 的许可证，它们也有可以配置的许可证特征，例如 copyleft 和署名。不过，要注意的是，-ND（no derivatives，禁止演绎）和 -NC（non-commercial，非商业性使用）这两个 Creative Commons 许可证变种，和自由软件是不兼容的，如果使用了这些资源，你的项目在自由软件生态里的能力就会受限。

[0]: https://creativecommons.org/

## 硬件

硬件项目（设计图、HDL 源等）建议使用<a href="https://cern-ohl.web.cern.ch/home" class="non-free" title="本链接指向非自由网站">CERN 开放硬件许可证（CERN Open Hardware License）</a>。

## 字体

建议使用 <a href="https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL" class="non-free" title="本链接指向非自由网站">SIL 开放字体许可证（SIL Open Font License)</a>来分发字体，它与自由软件相兼容。

## 文档

大多数项目不会为其文档选用特别的许可证。不过，[GNU 自由文档许可证（Free Documentation License）][fdl] 有时候也会用在这里。

[fdl]: https://www.gnu.org/licenses/fdl-1.3.html
