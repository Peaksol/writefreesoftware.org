---
title: Licensing non-software assets
weight: 10
---

Free software licenses are most suitable for licensing, well, software. However,
free software projects often incorporate media which is not software in and of
itself, such as artwork and documentation. Different licenses are recommended
for these use-cases. We have some recommendations for licenses which are
suitable for non-software media, are within the spirit of free software, and
are compatible with free software licenses.

## Creative Commons

Most multimedia assets -- images, audio, videos, writing, and so on -- are
suitable for use with [Creative Commons][0] licenses, which include configurable
options for traits such as copyleft and attribution. Note, however, that the -ND
(no derivatives) and -NC (non-commercial) variants of Creative Commons licenses
are incompatible with free software, and the use of these assets will limit the
utility of your project within the free software ecosystem.

[0]: https://creativecommons.org/

## Hardware

Hardware projects (schematics, HDL sources, etc) are encouraged to use the 
<a href="https://cern-ohl.web.cern.ch/home" class="non-free" title="This link will take you to a non-free website">CERN Open Hardware License</a>.

## Fonts

The
<a href="https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL" class="non-free" title="This link will take you to a non-free website">SIL Open Font License</a>.
is recommended for distributing fonts in a manner compatible with free software.

## Documentation

Most projects don't use a special license for their documentation. However,
the [GNU Free Documentation License][fdl] is occasionally used for this
purpose.

[fdl]: https://www.gnu.org/licenses/fdl-1.3.html
