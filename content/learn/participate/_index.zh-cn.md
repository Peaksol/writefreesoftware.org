---
title: 参与其中
weight: 2
---

有许多方法，能够参与到自由软件运动之中，包括发布自己的自由软件项目、贡献到已有的自由软件社区、为自由软件举办活动等等。

{{< button "/learn/participate/contribute" "了解如何做贡献" >}}{{< button "/learn/participate/publish" "了解如何发布" >}}
