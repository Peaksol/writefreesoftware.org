---
title: Getting involved
weight: 2
---

There are many ways to get involved with the Free Software movement, including
publishing your own free software projects, contributing to existing free
software communities, organizing events and activism for the cause of free
software, and more.

{{< button "/learn/participate/contribute" "Learn about contributing" >}}{{< button "/learn/participate/publish" "Learn about publishing" >}}
