---
title: Contribute to this website
weight: 1000
---

This website is itself free software. The source code for the content and layout
is available [on SourceHut][0], a free software development platform. Follow the
link for information on how to contribute improvements! The website and its
content uses the Creative Commons Attribution-ShareAlike 4.0 International
([CC-BY-SA 4.0][CC-BY-SA]) license, a copyleft license.

[0]: https://sr.ht/~sircmpwn/writefreesoftware.org/
[CC-BY-SA]: https://creativecommons.org/licenses/by-sa/4.0/

The theme is based on the Hugo
<a href="https://github.com/onweru/compose" class="non-free" title="Warning: this link will take you to GitHub, a non-free website">compose</a>
theme, which is free software licensed with the [MIT
license](https://mit-license.org/), copyright &copy; 2020 Weru.
