---
title: Contribute to free software
weight: 1
---

Scores of free software projects are looking for help. No matter your area of
expertise -- competence in any programming language, writing documentation,
community management, marketing skills, design and art skills -- there's a
project out there which could use your help. Look at the tools you use every
day: chances are that many of them are already free software.

Their communities are waiting to meet you! Find their chat rooms, internet
forums, and other hang-outs and introduce yourself.

## Things to know

When you submit improvements to free software, you will generally be expected to
provide them under the terms of the software license the project is already
distributed with. Review the license and ensure that you can agree to its terms
when licensing your own contributions.

If you don't own the work you are contributing (for example, you're contributing
on your employer's time or equipment), you need to secure the authorization of
the copyright holder. This also applies if you incorporate code from another
free software project -- see <nobr>[re-using free software][0]</nobr> for
more details.

[0]: /learn/participate/derived-works/

## Regarding "Contributor License Agreements"

Some projects will ask you to sign a "Contributor License Agreement", or a
similar document, when providing your contribution. It is important that you
read it carefully. This document may be used to simply verify that you own the
copyright over your change and have the right to contribute it. However, many of
these agreements may also ask you to waive some of your rights, for example to
permit the publisher to incorporate your changes into non-free versions of the
software.

You are not obligated to waive your rights. Such agreements may be required for
the original publisher to agree to incorporate your changes into their version
of the software, and to distribute those changes on your behalf. However, you
are always entitled to the right to distribute an improved version of the
software yourself, independently of the original publisher.

Publishers of free software are strongly discouraged from using Contributor
License Agreements to manage their community contributions. For more details,
see [managing copyright ownership][1].

[1]: /learn/participate/copyright-ownership/
