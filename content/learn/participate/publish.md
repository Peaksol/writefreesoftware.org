---
title: Publishing free software
weight: 2
---

Got a great idea for a new free software project and itching to publish it?
Wonderful! Here are the steps:

1. Write it!
2. Choose a software license
3. Publish your project
4. Build a community (optional)

## Writing your software

We can't help you much with this part, of course. However, one word of advice:
**publish early**. Many new maintainers are hesitant to publish their code,
worried that poor documentation, missing features, abundant bugs, or even just
poor code -- it all needs to be fixed before it's ready for the world. Not so!
Releasing early is a crucial step for getting rapid feedback and is essential to
attracting others to help. Get your code out there as soon as possible.

## Choosing a license

Before you publish, however, you need to choose a free software license that
suits your needs. You *can* write one yourself, but this is **strongly** advised
against, even for big corporations that can hire a lawyer to help. There are
plenty of licenses out there to suit every need, and we go over how to choose
the right one for you in [choosing a license][0].

[0]: /learn/participate/choose-a-license/

Choosing a license is **required** when publishing a free software project -- in
the absence of a license your work is non-free. Your choice may also have
significant long-term consequences for your project, so choose carefully. This
is an important step.

## Publishing your project

When you're ready to publish your work, the easiest way is to upload it to a
**software forge**. There are many to choose from, each offering numerous tools
to make development and collaboration easier for you. Many of these software
forges are themselves free software -- we recommend using these over the
non-free alternatives. Here are a few suggestions to check out:

- [Codeberg](https://codeberg.org)
- [SourceHut](https://sourcehut.org)

You can also host your project on your own infrastructure. Something as simple
as a tarball on a web server will do, but you can also run an entire software
forge in your own datacenter if that's better for your project.

<!--

Feel free to add forges here. Criteria for inclusion:

- Reasonably feature complete and useful for hosting projects with minimal fuss
- Hosted on stable infrastructure (self-hosted forges will not be considered)
- Trustworthy and transparent maintainership
- Free software, of course

Add new hosts in alphabetical order.

-->

## Building a community

One of the chief advantages of publishing free software is that your community
can get involved to help make it better than you can do alone. Managing your
community is important to make sure that your software is the best it can be.
[We publish articles][blog] on a variety of subjects of interest to free
software participants, including advice on community building and management.
Check it out!

[blog]: /blog/
