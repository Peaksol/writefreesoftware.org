---
title: 发布自由软件
weight: 2
---

想到了一个自由软件项目的好点子，已经迫不及待想发布它了？不错！接下来要走这些步骤：

1. 写出来！
2. 选择一份软件许可证
3. 发布项目
4. 建立社区（可选）

## 编写软件

这一部分，我们当然帮不到你。不过有一点建议：**趁早发布**。很多新的维护者，在发布代码的时候都会犹豫，害怕文档不好、功能缺失、漏洞百出或是代码劣质 -- 得把问题都修复了才能让它见光。未必！趁早发布很重要，这样能很快得到反馈，也能吸引他人来帮忙。尽早把代码发布出来吧！

## 选择许可证

不过，发布之前，你得选择一份合适的许可证。你*可以*自己写一份，不过**强烈**建议你不要这么做，就算是聘得起律师来协助的大公司，也不建议这样。有很多许可证可以满足各种需求，在[选择一份许可证][0]部分，我们讲解了怎么选择一份适合你的许可证。

[0]: /learn/participate/choose-a-license/

选择一份许可证，是发布自由软件项目**必要**的一步 -- 如果没有许可证，你的作品就是非自由的。你的选择，后续会对项目有显著的影响，所以选择应当慎重。这一步至关重要。

## 发布软件

当你准备好把作品发布出去了，最简单的方法就是把它上传到一个**软件炉（software forge）**里。可供选择的平台有很多，每一个都提供了大量工具，能简化开发和协作流程。许多软件炉本身也是自由软件 -- 我们建议用这些，而不是那些非自由替代品。下面有一些建议，值得一看：

- [Codeberg](https://codeberg.org)
- [SourceHut](https://sourcehut.org)

你也可以用自己的基础设施来托管你的项目。最简单的，在网页服务器上弄一个 tarball 就能实现，但你也可以在自己的数据中心运行一整个代码炉，如果这样更适合你的项目。

<!--

Feel free to add forges here. Criteria for inclusion:

- Reasonably feature complete and useful for hosting projects with minimal fuss
- Hosted on stable infrastructure (self-hosted forges will not be considered)
- Trustworthy and transparent maintainership
- Free software, of course

Add new hosts in alphabetical order.

-->

## 建立社区

发布自由软件的一大优点是，比起一己之力，社区的参与更能帮忙改进软件。要把软件做到做好，管理社区是一大要诀。我们就一系列可能感兴趣的主题，[发布了文章][blog]给自由软件的参与者，其中就有社区建立和管理的建议。快去看看吧！

[blog]: /blog/
