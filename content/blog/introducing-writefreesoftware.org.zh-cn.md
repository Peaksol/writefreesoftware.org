+++
author = "Drew DeVault"
title = "编写自由软件：自由软件运动的综合教育资源"
date = "2023-05-17"
description = "writefreesoftware.org 是一个新的网站，用于传播自由软件的哲学与实践"
tags = ["meta"]
image = "banner-blog.webp"
+++

今天，我很高兴宣布，[编写自由软件](/)发布了！这个网站是提供给自由软件社区的新资源，目的是为自由软件运动提供完整且易懂的入门及参考。我们的目的是，将自由软件的哲学与实践教授给他人，帮助人们理解自由软件为何重要、如何置于实践。

网站内容有很多，比如说：

- [什么是自由软件？](https://writefreesoftware.org/learn/)
- [自由软件许可证如何运作？](https://writefreesoftware.org/learn/licenses/)
- [Copyleft 如何运作？](https://writefreesoftware.org/learn/copyleft/)
- [如何选择自由软件许可证](https://writefreesoftware.org/learn/participate/choose-a-license/)
- [如何复用自由软件](https://writefreesoftware.org/learn/participate/derived-works/)

还有其它[众多内容](https://writefreesoftware.org/learn/)！


“编写自由软件”的主要内容部分，就自由软件如何运转，以及如何运用它的准则这方面，提供了很多有用的资源，但自由软件远不止于此。这个[博客][1]，尽管刚刚开始，但将提供更多独创资源，来帮助维护者及贡献者理解，如何更有效地与自由软件工作。

这个博客以后还会谈论到：

[1]: /blog

- 构建及管理自由软件社区
- 特定自由软件许可证解读
- 商业参与自由软件
- 将 copyleft 付诸实践的重要性

想要在这方面帮忙，或者有其它想法？
[考虑帮我们写作！](https://sr.ht/~sircmpwn/writefreesoftware.org/)
