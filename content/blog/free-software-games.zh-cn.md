+++
author = "Drew DeVault"
title = "自由软件电子游戏发布完全指南"
date = "2021-03-23"
description = "一部指导你将电子游戏发布为自由软件的完全指南"
tags = ["games", "publishers"]
image = "supertuxkart.jpg"
+++

*本文改编并再发布自 [Drew DeVault 的博客](https://drewdevault.com/2021/03/23/Open-sourcing-video-games.html)。*

电子游戏，是颇为有趣的一大软件门类。不同于大多数软件，电子游戏是一种创造性的工作，而非一种实用工具。大多数软件呼吁新功能，目的是满足用户的实际需求，而电子游戏呼吁新功能，却旨在展现创作者的创意愿景。同样，为了尽早交付成品，常常要将代码重构及偿还技术债等事务置于次要地位。自由软件带来的协作优势，对电子游戏而言并不明显。也许正是出于这些原因，属于自由软件的商业化游戏才寥寥无几。

不过，还是能找到这类游戏的一些例子的，它们在游戏领域也都产生了极大的影响。Id 在这方面很有名，他们将 DOOM 数个版本的源代码，都发布了出来。Quake 引擎也是以 GPL 发布的，并在随后产生了巨大的影响。它成为了许多游戏的基础，包括《半条命》系列等久盛不衰的作品。多亏了自由软件游戏发布者的慷慨贡献，一大批游戏大作才得以问世。

将游戏发布为自由软件，也是出于历史保护的需要。专有游戏往往衰落。全盛期过后，时间一长，由于适用平台稀缺、实体副本难得，许多游戏都会缓慢而又静悄悄地死去，遗忘于时间的长河。有些游戏则通过发布源代码，克服了这种问题，令其粉丝得以将游戏轻松移植到新平台，并保持其生命力。

你的游戏会拥有怎样的历史？是被完全遗忘，无法运行于现代平台？是成为有源软件，偶尔对忠实玩家有用，除此之外便再无什么别的影响？可能它也会像 DOOM 一样，永远活在数百种设备和操作系统的移植版中。可能它还会像 Quake 一样，其灵魂永远驻于未来深受喜爱的经典作品之中。如果你把源代码封闭下去，那定论就是第一条：享受完，便忘却。

出于这样的考虑，你要怎样保障游戏的历史？

## 有源：最低限度

最低的限度是，让你的游戏成为“有源”软件。注意，这并不等同于让它成为自由或开源软件！这类游戏中，同样这么做且比较有名的有《异形 3》《文明 4》《文明 5》《孤岛危机》《杀出重围》《波斯王子》《虚幻竞技场》和《弹弹跳跳闪避人》。

这种方法，将使你的源代码允许查看，可能还允许编译及运行，但禁止衍生作品。比起闭源，这么做无疑是更好的：这为模组开发者、速通玩家等粉丝提供了有用的资源；忠实玩家也许还能以它为基础，让游戏在以后其它的平台上运行，尽管只能独自这么做，而不能分享其作品。

如果你在执法方面睁一只眼闭一只眼，那么一些玩家最终便可以分享其作品，但你会让他们的法律依据不稳固。如果你对自己的知识产权有很强的保护欲，但又认为这会限制游戏潜在的第二次生命，这时我才会推荐这么做。

## Copyleft 配专有资源

下一步是使用 copyleft 许可证，让你的游戏成为自由软件，但阻止该许可证延伸至游戏资源 &mdash; 任何人如果想让源代码工作，那就需要购买游戏并提取资源，或者提供自己社区制作的资源。这种方法在自由软件游戏中颇为流行，利多弊少。你也能列入我们 DOOM 和 Quake 的例子，以及《失忆症：黑暗后裔》《网络奇兵》《毁灭公爵 3D》和《德军总部 3D》。

由于其软件更加容易移植到新平台并分享给他人，这类游戏的寿命更为长久。DOOM 能够在手机、数码相机、ATM 机上，甚至在烤面包机上跑起来！原开发者无需任何持续投入，便能保障其历史。这也允许了衍生作品 &mdash; 基于你的代码的新游戏 &mdash; 的出现，虽说这也可能会劝走一部分开发者。使用诸如 [GPL](https://www.gnu.org/licenses/gpl-3.0.en.html) 的 copyleft 许可证，要求衍生作品同样发布为自由软件。一般来说，社区对此不会有什么意见，但这可能会影响后续开发者将你的作品并入其自己的商业游戏的意愿。我个人认为，使用 copyleft 许可证会促进自由软件的增长，而这是件好事 &mdash; 但你可能还想考虑另一种方法。

## 宽松型许可证配专有资源

如果你想让你的源代码，以后成为尽可能多的游戏的一部分，那么 [MIT][0] 等宽松型自由软件许可证，就是一条途径。走了这种方法的游戏，[《星系舰队》](https://github.com/blendogames/flotilla)就是一个例子。这样可以允许开发者将你的源代码并入自己的游戏，而几乎不受什么限制，既可以直接衍生出新作品，又可以从你的代码中取出一些样例，然后并入自己的项目。这种做法，不会给开发者带来义务，要求其以同样的方法发布其自己的改动或作品：开发者可以直接获取它，再加入几行字符串即可。采取这种方法，能使之轻松并入新的商业游戏。

[0]: https://opensource.org/licenses/MIT

这样发布自己的代码，是最无私的做法。如果你不在意以后你的代码会发生什么，并且只是想让它成为自由软件下去，那我就会建议这样。虽说这样做无疑能让以后大量的项目使用你的作品，但 copyleft 方法更能确保以后尽可能多的软件*也能*成为自由软件。

## 开放资源

如果你更为慷慨，你也可以将资源发布出来。发布这些资源，比较好的许可证包括 [Creative Commons](https://creativecommons.org/) 许可证家族。所有这些许可证都允许资源的自由传播，如此一来，以后的玩家就不必再购买游戏才能拿到资源。如果你使用的分发平台不复存在了，或者因为你不在了而买不到了，则这一点尤为重要 &mdash; 如果你宁愿随着游戏变得老旧，也要在不断减少的资源销售额中，保留自己的一部分，那这项决策就要考虑清楚了。

使用 Creative Commons，也能让你调整你在多大程度上允许资源的复用。你可以选择不同的 CC 许可证，从而控制资源的商业化及衍生作品的使用。如果要只允许自由分发，而禁止其它任何行为，则 CC-NC-ND 许可证（非商业性使用，禁止演绎）可以做到这一点。CC-BY-SA 许可证是 copyleft 的 Creative Commons 许可证：它允许自由分发、商业化及衍生作品，*前提是*衍生作品在分发时授予同样的权利。宽松的分发方法是使用 CC-0，相当于将资源发布到了公有领域。

允许资源的衍生及再商业化，可以为新游戏的开发者，尤其是预算较低的独立开发者，省下不少的时间。这也可以促进衍生*游戏*的开发，这类似于模组开发，玩家可以发挥创意，对你的资源进行再混合，从而制作出新游戏或是扩展包。

## 如果我对我的游戏没有完整所有权呢？

如果你没有所有权，那么就不能授权。如果你依赖专有库或是第三方关卡编辑器，或者你没有音乐或精灵图的版权，那么你就不能让它自由或开源。

这时，我建议你将能发布的一切，都发布成自由软件。这可能意味着你发布了一款残缺的游戏 &mdash; 没有这些资源，它可能根本不能玩，甚至不能编译。虽然很不幸，但尽量把资源发出来，有利于社区自身来填补游戏的空缺。他们也许能重构你的代码来解决这些问题，也许还能用自由的替代品来取代专有的部分。这也使得你的游戏中开放的一部分，能在未来的游戏中得以复用。

## 但它可以拿来作弊！

没错。而且值得一提的是，如果你的游戏有联网组件，必须要依赖你的服务器运行，那么将它发布为自由软件几乎没什么意义，特别是当你最终关闭了那些服务器时。

这里要做出一个权衡。实际上，要在你的游戏中防止作弊是十分困难的。如果你开发了一款十分流行的多人竞技游戏，那你我都知道，就算尽了最大努力，还是会有人利用它来作弊的。将它保持为专有软件，并不能避免作弊者。更好的方法是，采用社区化的解决方案 &mdash; 例如采用作弊举报系统，或者让好友在私人服务器上游玩。

让你的游戏成为自由软件，也可能帮到那些技术没那么娴熟的脚本小子，让他们发现怎样更容易地在游戏中作弊。我没法替你决定这种权衡值得与否，但我知道的是，将游戏开放出来，其好处是巨大的，而将它封闭来阻止作弊，其效果令人质疑。

## 但我的代码写得很烂！

大家的都一样。🙂 我们都知道，游戏都是紧赶工期的，把代码写整洁并非头等大事。我向你保证，你的社区只会忙着玩乐，而无暇评价你的代码质量。有许多项目，本该成为自由软件，结果都因为想着先“清理”一下代码，而走向了死亡。如果你有这种想法，那么你永远都不会感到满意，从而永远都不会将它公开。我向你保证：不管你的游戏处于什么状态，它都准备好成为自由软件了。

附言：Ethan Lee 提示了我《弹弹跳跳闪避人》里留有很多糟糕透顶的代码，你可以在 [2.2 标签][vvvvvv 2.2]自由地浏览。这些代码写得并不好，但你多半都不曾知道过 &mdash; 你只记得《弹弹跳跳闪避人》是一款倍受赞誉的游戏。游戏开发者是在诸多严格的限制下工作的，没人会借此而批评他们 &mdash; 我们只顾着玩乐！

[vvvvvv 2.2]: https://github.com/TerryCavanagh/vvvvvv/tree/2.2

## 那我该做什么？

来把具体步骤列出来吧。首先，你要回答以下问题：

- 我确实拥有整个游戏吗？我可以将哪些部分发布为自由软件？
- 我发布代码的形式，是有源、copyleft，还是宽松许可？
- 资源呢？是专有？还是 Creative Commons？如果是后者，那是哪个版本？

如果你不确定怎么选最好，那我建议是，对代码使用 GPL，对资源使用 CC-BY-SA。这样可以允许衍生作品的发布，只要它是用类似许可证开放的。这能允许社区在你作品的基础上进行构建、将其移植到新平台、构建繁荣的模组社区，并自由分享你的资源，进而保证你的游戏历史延续。如果你想自己做细节上的决定，那就再读一读上面的意见，选出你对各项要采用的许可证，然后再继续。

如果你在这些步骤中有哪一步需要帮忙，或者有问题要问，请[发邮件给我](mailto:sir@cmpwn.com)，我会尽我所能帮助你。

**发布源代码**

准备一份你源代码的归档，并添加许可证文件。如果你选择发布为有源软件，则只需在名为 LICENSE 的文本文件，写上“版权所有 © <你的名字> <当前年份>。保留所有权利。”如果你选择其它方法，则将许可证文本复制到 LICENSE 文件。

如果你想要快速完成这一步，只需要将代码和许可证打包成 zip 文件或者 tarball，然后将它丢到你的网站上去。如果你有耐心，那更好的方法就是将它发布成 git 仓库。如果你已经在用版本控制系统了，你可能得好好考虑一下是否要将完整的版本控制历史发布出去 &mdash; 这个问题的回答可能为“是”，但如果你不确定，那多半为“否”。只要把代码复制下来，删除 .git 仓库，然后视需求将它导入新的仓库。

再次检查一下，确保你没有包括任何制品（artifact）&mdash; 资源、可执行文件、库等 &mdash; 然后将它 push 到你选择的托管服务。GitHub 是个很流行的选择，但我也会自私地推荐 [sourcehut](https://sourcehut.org)。如果你有实现，再写一个简单的 README 文件作项目介绍。

**发布资源**

如果你决定让资源保持专有，则没有后续步骤了。玩家可以搞明白怎么从购买的游戏中提取资源。

如果你决定将其开放出来，那么准备一份资源的归档。在其中包含一份你选择的许可证的副本 &mdash; 例如你使用的 Creative Commons 许可证 &mdash; 然后将它丢入一份 zip 文件或 tarball 之类的，并放在你的网站上。如果你足够慷慨，那么可以写一点指南，说明如何在玩家编译代码时将资源包并入游戏。

**告诉大家！**

告诉所有人，你让你的游戏成为自由软件了！写一篇简单的博文，给出源代码和资源的链接。接着，在媒体和社区对你贡献的感激之下，享受被聚光灯照射的感觉吧。

就这一点，最后还有一个请求：如果你选择发布为有源软件，请在公共陈述中说清楚这一点。有源软件与“自由软件”或“开源软件”*并非*同一回事，这个区别很重要。

现在，轮到我感谢你了：看到你将你的游戏发布为自由软件，我十分高兴！社区能做的贡献，比你的要多得多。希望你的游戏在接下来的几年能继续存活下去，存活在自己的模组和移植里，也存活在对以后游戏所做贡献的灵魂里。你做了一件伟大的事情。谢谢你！
