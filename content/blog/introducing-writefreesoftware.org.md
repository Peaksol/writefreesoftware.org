+++
author = "Drew DeVault"
title = "Write Free Software: A comprehensive educational resource for the Free Software movement"
date = "2023-05-17"
description = "writefreesoftware.org is a new website for disseminating free software philosophy and practice"
tags = ["meta"]
image = "banner-blog.webp"
+++

I'm pleased to announce the publication of [Write Free Software](/) today! This
website is a new resource for the Free Software community which aims to provide
a comprehensive and accessible entry-point and reference for the free software
movement. We aim to educate others on free software philosophy and practice, to
help people understand why free software is important and how to put it into
practice.

Come here to learn things like:

- [What is free software?](https://writefreesoftware.org/learn/)
- [How do free software licenses work?](https://writefreesoftware.org/learn/licenses/)
- [How does copyleft work?](https://writefreesoftware.org/learn/copyleft/)
- [How to choose a free software license](https://writefreesoftware.org/learn/participate/choose-a-license/)
- [How to re-use free software](https://writefreesoftware.org/learn/participate/derived-works/)

And [much more](https://writefreesoftware.org/learn/)!

The main content area of Write Free Software provides a lot of useful
informational resources about how free software works and how its principles can
be applied, but that's not all there is to free software. The [blog][1], though
it's just getting started, will provide a lot more free-form resources to help
maintainers and contributors better understand how to work effectively in free
software.

Future topics for the blog include:

[1]: /blog

- Building and managing your free software communities
- Breakdowns of specific free software licenses
- Commercial participation in free software
- The importance of copyleft in practice

Want to help with these or any other ideas?
[Consider writing for us!](https://sr.ht/~sircmpwn/writefreesoftware.org/)
