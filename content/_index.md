+++
title = "Write Free Software"
+++
{{< block "grid-2" >}}
{{< column >}}

# We build software for everyone

Free software is a global collaborative movement to build the world's best
software in a manner that respects the essential rights and freedoms of its
users and authors &mdash; the freedom to **use**, **study**, **improve**, and
**share** your software.

{{< button "learn" "What is free software?" >}}{{< button "learn/participate" "Get started" >}}
{{< /column >}}

{{< column >}}
![Picture of hackers at FOSDEM](/images/banner.jpg)
<small>Photo by Flo Köhler, CC-BY 3.0</small>
{{< /column >}}

{{< /block >}}
