# Write Free Software

[writefreesoftware.org](https://writefreesoftware.org) is a resource for
introducing people to free software philosophy and helping them put it into
practice.

## Contributing

Please contribute improvements via email to
[the mailing list](https://lists.sr.ht/~sircmpwn/writefreesoftware.org) using
[git send-email][0].

[0]: https://git-send-email.io

## Content guidelines

What kind of content is welcome on Write Free Software?

### Main content

The main content is narrowly focused on the following topics:

- Introduction to free software philosophy
- Informational resources on free software in practice, e.g. using licenses

The main content should not endorse any specific free software projects other
than those relevant to the development of free software itself (e.g. free
software licenses, software forges, etc), and should generally avoid using them
as examples.

### Blog

The blog is open to any contributors, and you are encouraged to write a post
about free software for it :)

The scope of the blog is limited to matters related to free software philosophy
and the implementation of free software, but the scope is not so strict as the
main content. Details regarding specific projects and communities and their
approach to implementing free software practices, community management, and so
on, are welcome here.

### Style guidelines

- Use "free software" rather than "open source" unless specifically referring
  to the open source movement

## Translations

Want to translate writefreesoftware.org into your language? Add an entry to
config.toml for your language, then you can translate `content/whatever.md` by
writing up e.g. `content/whatever.fr.md`.

As new translations are large changes, please send them to the mailing list
using git request-pull rather than sending individual patches.

## Code of conduct

Participants are expected to respect the [Contributor Covenant Code of
Conduct][coc]. Conduct questions and problem reports should be directed to [Drew
DeVault](mailto:sir@cmpwn.com).

[coc]: https://www.contributor-covenant.org/version/2/1/code_of_conduct/
